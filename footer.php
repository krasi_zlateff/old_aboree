<div id="footer">
   <div class="container">
      <div class="copyright">
         <a class="logo" href="/" title="Welcome to aboree">
            <span class="icon"></span>
            <span class="site-title">aboree</span>
         </a>
         <ul class="terms-of-use">
            <li><a href="#">Terms of use</a></li>
            <li><a href="#">Privacy</a></li>
            <li><a href="#">Jobs</a></li>
         </ul>
      </div>
   </div>      
</div>
<script src="/source/js/jquery.js"></script>
<script src="/source/js/main.js"></script>
</body>
</html>