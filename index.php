<!DOCTYPE html>
<html>
<head>
<title>Welcome to aboree</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
<link href="/stylesheets/print.css" media="print" rel="stylesheet" type="text/css" />
<!--[if IE]>
  <link href="/stylesheets/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
   <div class="main-wrapper">
      <div class="front-containter">
         <div class="image-wrapper">
            <img class="main-image" src="/source/images/background.jpg">
         </div>
         <div class="dots">
            <div class="container">
               <div class="home-wrapper">
                  <div class="welcome">
                     <h1 class="welcome-notice">Welcome to aboree</h1>
                     <span class="welcome-slogan">let's make aboree tonight</span>
                  </div>
                  <div class="user-wrapper">
                     <?php include dirname(__FILE__).'/login.php'; ?>
                     <?php include dirname(__FILE__).'/register.php'; ?>
                     <ul class="user-login">
                        <li><a class="login" href="#">Login</a></li>
                        <li><a class="signup" href="#">Join</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php require dirname(__FILE__).'/footer.php';?>
