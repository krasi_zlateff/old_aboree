$(document).ready(function(){
   $(".user-wrapper .login").on("click", function(){
      $(".user-login").animate({opacity: "toggle"}, 500)
      $(".login-form-block").animate({opacity: "show"}, 500)
   })
   $(".user-wrapper .signup").on("click", function(){
      $(".user-login").animate({opacity: "toggle"}, 500)
      $(".register-form-block").animate({opacity: "show"}, 500)
   })
   $(".user-wrapper .login-form-block .signup").on("click", function(e){
      e.preventDefault();
      $(".user-login").css("display", "none")
      $(".login-form-block").animate({opacity: "toggle"}, 500)
   })
})